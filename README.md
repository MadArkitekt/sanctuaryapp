<h1>Sanctuary</h1>
<img src="https://raw.githubusercontent.com/MadArkitekt/MadArkitekt.github.io/master/assets/images/customsanct.png" >
<h2>If ears had eyes...they'd show the way to <em>Sanctuary</em></h2>
<h3>Brief Overview</h3>
<p><h4>Sanctuary provides you with a very special service: coupling anonymous geolocation data with sound level readings, by displaying the previous information on a map using color-coded marker, allows users to literally <em>see sound</em>. With the capability to see sound, finding a quiet place to study or a local concert, whatever sanctuary you seek, becomes trivial.</h4>
</p>
<p>
1. With <em>Sanctuary</em>, you log into, or sign-up for, a free-of-charge remote database (Emails are NOT collected for commercial purposes)<br>
2. Your iPhone, pending your approval of <em>Sanctuary's</em> access to your iPhone's location services (necessary for <em>Sanctuary</em> to function properly) begins receiving location updates.<br>  
3. Once your location is verified, the hybrid sattellite/traditional map you see in the screenshot below, begins filling the latest uploaded information from <em>Sanctuary's</em> database. Each new location/sound level set of data is represented on the map with an appropriatedly colored marker.<br>  
4. Sound levels are recorded by pressing the "Meter" button, then the "Upload" button in the bottom-right corner. If there are any complications, the app includes alert messages explaining how to make any necessary corrections and proceede with finding <em>Sanctuary</em></p>
<hr>
<img src="https://raw.githubusercontent.com/MadArkitekt/MadArkitekt.github.io/master/assets/images/sanctMap@2x.png">
(<em>Sanctuary</em> in 'Map' mode)
<hr> 
<h3>Support</h3>
<p>I stand behind what I build, if it's not functioning correctly, your login no longer works correctly, you think the sound level meter is out of calibration, feel free to <a href="https://github.com/MadArkitekt/SanctuaryApp/issues/new">let me know</a>.<br>
Finally, if you have any suggestions for new features or improvements you'd like to see, or if you are enjoying using the app.
</p>
<hr>
<h1>LEGAL WAIVER AND TERMS OF USE</h1>
<p>
- (DO NOT USE THIS APPLICATION WHILE CONDUCTING A VEHICLE, MOTORIZED OR POWERED BY ANY OTHER MEANS!)<br>
- I, the end user of the iOS application "Sanctuary", created and maintained by Edward Salter, do hereby knowingly absolve Mr. Salter of any and all legal liability for injuries that may occur from improper use of this application.
</p>
